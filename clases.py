"""                author: "Angel Morocho"  "Valeria Guerrero" "Karen Paladines"
             email: "angel.m.morocho.c@unl.edu.ec" "valeria.guerrero@unl.edu.ec" "karen.paladines@unl.edu.ec"
Ejercicio 1:
            Elabore tres clases con codigo Python, con el tema de su recurso educativo grupal"""
class geometria:
    """    Define el area de polígono según su base y su altura    """
    def _init_(self, base, altura):
        self.base = base
        self.altura = altura
class Rectangulo(geometria):
    def area(self):
        self.area = self.base * self.altura
        return self.area
    def str(self):
        return f"mul {self.area}"
class Triangulo(geometria):
    def area(self):
        self.area = (self.base * self.altura) / 2
        return self.area
    def str(self):
        return f"mul {self.area}"
rectangulo = Rectangulo(20, 10) # Estos son valores refernciales para comprobar el ejercicio
triangulo = Triangulo(20, 12) # Estos son valores refernciales para comprobar el ejercicio
print("Área del rectángulo: ", rectangulo.area())
print("Área del triángulo:", triangulo.area())

